package org.kitteh.bans;

import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Security;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Main {

    private static KeyPair keys;
    private static ServerSocket server;
    private static final HashMap<InetAddress, Long> recentConnections = new HashMap<InetAddress, Long>();
    private static final ExecutorService pool = Executors.newFixedThreadPool(10);
    private static final Random key = new Random();
    private static final String salt = "pepper";

    public static void main(String[] args) throws Exception {
        System.out.println("Generating key pairs");
        Security.addProvider(new BouncyCastleProvider());
        keys = KeyPairGenerator.getInstance("RSA").generateKeyPair();
        server = new ServerSocket(25565);
        System.out.println("Server started on *:" + server.getLocalPort());
        while (true) {
            try {
                Socket connection = server.accept();
                Long last = recentConnections.get(connection.getInetAddress());
                recentConnections.put(connection.getInetAddress(), System.currentTimeMillis());
                if (last != null && System.currentTimeMillis() - last < 300) {
                    connection.close();
                    System.out.println(connection.getInetAddress() + " disconnected due to flood protection");
                } else {
                    connection.setSoTimeout(2000);
                    System.out.println(connection.getInetAddress() + " has connected to the server");
                    pool.submit(new Connection(connection));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private static byte[] readKey(DataInputStream in) throws IOException {
        short len = in.readShort();
        byte key[] = new byte[len];
        in.read(key);
        return key;
    }

    private static String readString(DataInputStream in) throws IOException {
        int len = in.readShort();
        char[] chars = new char[len];
        for (int i = 0; i < len; i++) {
            chars[i] = in.readChar();
        }
        return new String(chars);
    }

    private static void writeKey(DataOutputStream out, byte[] key) throws IOException {
        out.writeShort(key.length);
        out.write(key);
    }

    private static void writeString(DataOutputStream out, String str) throws IOException {
        out.writeShort(str.length());
        out.writeChars(str);
    }

    private static class Connection implements Runnable {

        private final Socket client;

        protected Connection(Socket client) {
            this.client = client;
        }

        @Override
        public void run() {
            try {
                DataInputStream in = new DataInputStream(client.getInputStream());
                DataOutputStream out = new DataOutputStream(client.getOutputStream());
                String message;
                String username = null;
                //
                if (in.read() != 0x02) {
                    message = "BanConcept verification server";
                } else {
                    //
                    in.readByte();
                    username = readString(in);
                    readString(in);
                    in.readInt();
                    //
                    String hash = Long.toString(key.nextLong(), 16);
                    //
                    out.writeByte(0xFD);
                    writeString(out, hash);
                    writeKey(out, keys.getPublic().getEncoded());
                    //
                    in.read();
                    //
                    Cipher cipher = Cipher.getInstance("RSA");
                    cipher.init(Cipher.DECRYPT_MODE, keys.getPrivate());
                    MessageDigest digester = MessageDigest.getInstance("SHA-1");
                    for (byte[] bit : new byte[][]{hash.getBytes("ISO_8859_1"), new SecretKeySpec(cipher.doFinal(readKey(in)), "AES").getEncoded(), keys.getPublic().getEncoded()}) {
                        digester.update(bit);
                    }
                    BufferedReader mcAuth = new BufferedReader(new InputStreamReader(new URL("http://session.minecraft.net/game/checkserver.jsp?user=" + URLEncoder.encode(username, "UTF-8") + "&serverId=" + URLEncoder.encode(new BigInteger(digester.digest()).toString(16), "UTF-8")).openStream()));
                    String reply = mcAuth.readLine();
                    mcAuth.close();
                    //
                    if (reply.equals("YES")) {
                        message = "Your unique code is: " + new sun.misc.BASE64Encoder().encode(digester.digest((username + salt).getBytes("UTF-8"))).substring(0, 8);
                    } else {
                        message = "Account not premium! mbaxter will eat your face!";
                    }
                }
                out.writeByte(0xFF);
                writeString(out, message);
                System.out.println(client.getInetAddress() + ((username != null) ? " [" + username + "]" : "") + " kicked with: " + message);
                client.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
